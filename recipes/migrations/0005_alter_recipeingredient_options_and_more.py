# Generated by Django 4.1.7 on 2023-02-28 22:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0004_rename_ingredient_recipeingredient'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='recipeingredient',
            options={'ordering': ['food_item']},
        ),
        migrations.AlterField(
            model_name='recipeingredient',
            name='amount',
            field=models.TextField(max_length=100),
        ),
        migrations.AlterField(
            model_name='recipeingredient',
            name='food_item',
            field=models.TextField(max_length=100),
        ),
    ]
